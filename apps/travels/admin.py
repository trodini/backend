from django.contrib import admin

from models import Travel, Location

admin.site.register(Travel)
admin.site.register(Location)
