import math

from django.db import models
from geoposition.fields import GeopositionField

from apps.users.models import User


class Location(models.Model):
    '''
    Location model to know the lat long and area name of a location
    '''
    lat_long = GeopositionField(blank=True)
    area_name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)

    @property
    def latitude(self):
        return self.lat_long.latitude

    @property
    def longitude(self):
        return self.lat_long.longitude

    # On Python 3: def __str__(self):
    def __unicode__(self):
        return self.area_name


class TravelManager(models.Manager):
    pass


class Travel(models.Model):
    '''
    Travel model to store and calculate possible routes
    a user can take
    '''

    start_time = models.TimeField()
    end_time = models.TimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    place_of_departure = models.ForeignKey(
        Location, related_name="departures")
    place_of_arrival = models.ForeignKey(
        Location, related_name="arrivals")
    user = models.ForeignKey(User, related_name="travels")
    price = models.DecimalField(max_digits=7, decimal_places=2)

    objects = TravelManager()

    def get_distance(self):
        """
        Calculate the distance between the arrival and departure locations.
        - based on Haversine forumula

        http://www.movable-type.co.uk/scripts/latlong.html

        Decimal Degrees = Degrees + minutes/60 + seconds/3600
        """
        lat1 = float(self.place_of_departure.latitude)
        lon1 = float(self.place_of_departure.longitude)
        lat2 = float(self.place_of_arrival.latitude)
        lon2 = float(self.place_of_arrival.longitude)

        r = 6371  # km, radius of the earth
        diff_lat = math.radians(lat2 - lat1)
        diff_lon = math.radians(lon2 - lon1)
        lat1 = math.radians(lat1)
        lat2 = math.radians(lat2)
        a = (
            math.sin(diff_lat / 2) * math.sin(diff_lat / 2) +
            math.sin(diff_lon / 2) * math.sin(diff_lon / 2) *
            math.cos(lat1) * math.cos(lat2))
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
        d = r * c

        return d

    # On Python 3: def __str__(self):
    def __unicode__(self):
        return '{} to {}'.format(
            str(self.place_of_departure), str(self.place_of_arrival))
