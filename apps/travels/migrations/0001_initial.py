# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import geoposition.fields


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lat_long', geoposition.fields.GeopositionField(max_length=42)),
                ('area_name', models.CharField(max_length=255)),
                ('description', models.TextField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Travel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_time', models.TimeField()),
                ('end_time', models.TimeField()),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField()),
                ('price', models.DecimalField(max_digits=7, decimal_places=2)),
                ('place_of_arrival', models.ForeignKey(related_name='arrivals', to='travels.Location')),
                ('place_of_departure', models.ForeignKey(related_name='departures', to='travels.Location')),
                ('user', models.ForeignKey(related_name='travels', to='users.User')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
