from django.test import TestCase

from models import User


class UserTestCase(TestCase):
    '''
    Testing the user model
    '''

    def set_up(self):
        gee = User("ger@meltwater.org", "Gerald", "Pharin", "password")
        bright = User("bright@meltwater.org", "Bright", "Ahedor", "password2")
        david = User("david@meltwater.org", "David", "Sleek", "password3")

        return (gee, bright, david)
