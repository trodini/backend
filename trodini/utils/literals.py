'''
Status codes:

First Number: 9 - This represents PayLittle Errors
Second Number: 1 - For code relating to users app.
               2 - For code relating to identity app.
               3 - For code relating to utils
Third Number: 2 - For success messages
              3 - For neither success or error messages
              4 - For error messages
Fourth Number: 0-7 - Serial numbering of Errors
               8-9 - Serial numbering from parse errors

'''


'''
Messages
'''


'''
Strings
'''

CONTACT_NO_STRING = 'contact_no'
ID_NO_STRING = 'id_no'
NAME_STRING = 'name'
FIRST_NAME_STRING = 'first_name'
LAST_NAME_STRING = 'last_name'
PASSWORD_STRING = 'password'
RESULTS_STRING = 'results'
DETAILS_STRING = 'details'
EMAIL_STRING = 'email'
AVATAR_STRING = 'avatar'
USER_DOES_NOT_EXIST_STRING = 'User does not exist'
USER_STRING = 'user'
USERS_STRING = 'users'
DATE_CREATED_STRING = 'date_created'
DATE_COMPLETED_STRING = 'date_completed'
AMOUNT_STRING = 'amount'
TIMESTAMP_STRING = 'timestamp'
IMAGE_STRING = 'image'
USER_NAME_STRING = 'user_name'
STATUS_STRING = 'status'
TOKEN_STRING = 'token'
SM_ID_STRING = 'sm_id'

PICTURE_STRING = 'picture'

TOO_SMALL = 'too small'
TOO_LARGE = 'too large'
DEFAULT = 'default'
EXISTS = 'exists'
NAN = 'NaN'
USER_MODEL_STRING = 'User'


'''
User Response messages
'''

USER_LOGIN_ERROR = {
    DETAILS_STRING: 'Error logging user in',
    STATUS_STRING: 91400
}

USER_LOGOUT_ERROR = {
    DETAILS_STRING: 'Error logging user out',
    STATUS_STRING: 91401
}

USER_SIGNUP_ERROR = {
    DETAILS_STRING: 'Error signing user in',
    STATUS_STRING: 91402
}

USER_SETTINGS_ERROR = {
    DETAILS_STRING: 'Error changing user settings',
    STATUS_STRING: 91403
}

USER_LOGOUT_SUCCESS = {
    DETAILS_STRING: 'Successfully logged user out',
    STATUS_STRING: 91200
}

USER_LOGIN_SUCCESS = {
    DETAILS_STRING: 'Successfully logged user in',
    STATUS_STRING: 91201
}

USER_SIGNUP_SUCCESS = {
    DETAILS_STRING: 'Successfully signed user in',
    STATUS_STRING: 91202
}

USER_SETTINGS_SUCCESS = {
    DETAILS_STRING: 'Successfully changed user settings',
    STATUS_STRING: 91203
}

NO_PASSWORD_SIGNUP = {
    DETAILS_STRING: 'Anonymous user created. - No password given',
    STATUS_STRING: 91405
}

GET_USER_ERROR = {
    DETAILS_STRING: 'Error getting user',
    STATUS_STRING: 91406
}

USER_EXISTS_ERROR = {
    DETAILS_STRING: 'User exists',
    STATUS_STRING: 91409
}

MAXIMUM_TOKEN_TRIES_ERROR = {
    DETAILS_STRING: 'Sorry you\'ve exceeded the max no. of tries',
    STATUS_STRING: 91411
}

USER_DOES_NOT_EXIST_ERROR = {
    DETAILS_STRING: 'User does not exist',
    STATUS_STRING: 91414
}

USER_UPDATE_PASSWORD_ERROR = {
    DETAILS_STRING: 'Error updating user\'s password',
    STATUS_STRING: 91415
}

USER_HAS_NOT_SIGNED_UP_ERROR = {
    DETAILS_STRING: 'User has not signed up successfully',
    STATUS_STRING: 91416
}

SEND_USER_PASSWORD_ERROR = {
    DETAILS_STRING: 'Error sending user password',
    STATUS_STRING: 91417
}

SET_PASSWORD_ERROR = {
    DETAILS_STRING: 'Error setting user password',
    STATUS_STRING: 91417
}

PERMISSION_DENIED_ERROR = {
    DETAILS_STRING: 'You dont have permission to do this',
    STATUS_STRING: 91418
}
