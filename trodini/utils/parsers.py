from rest_framework.response import Response
from rest_framework import status

from trodini.utils import literals


def get_parse_code(initial_code, parse_suffix):
    initial_code = str(initial_code)[:-2]
    code = initial_code + str(parse_suffix)
    return int(code)


def parse_error(err, param, default_message):
    response = {}
    if len(err.args) > 1 and err.args[1] is not None:
        if err.args[0] == literals.DEFAULT:
            return Response(
                {literals.DETAILS_STRING: 'Invalid {0} {1}'.format(
                    err.args[1], param)},
                status=status.HTTP_400_BAD_REQUEST)

        if err.args[0] == literals.TOO_SMALL:
            return Response(
                {literals.DETAILS_STRING: '{0} {1} length is too small'.format(
                    err.args[1], param)},
                status=status.HTTP_400_BAD_REQUEST)

    if err.args[0] == literals.DEFAULT:
        return Response(
            {literals.DETAILS_STRING: 'Invalid {}'.format(param)},
            status=status.HTTP_400_BAD_REQUEST)

    if err.args[0] == literals.TOO_SMALL:
        return Response(
            {literals.DETAILS_STRING: '{} length is too small'.format(param)},
            status=status.HTTP_400_BAD_REQUEST)

    if err.args[0] == literals.NAN:
        return Response(
            {literals.DETAILS_STRING: '{} is not a number'.format(param)},
            status=status.HTTP_400_BAD_REQUEST)

    if err.args[0] == literals.EXISTS:
        return Response(
            {literals.DETAILS_STRING: '{} already exists'.format(param)},
            status=status.HTTP_400_BAD_REQUEST)

    response.update(default_message)
    return Response(
        response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
