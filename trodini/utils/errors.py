from django.core.exceptions import ValidationError


class NameError(ValidationError):
    pass


class EmailError(ValidationError):
    pass


class UserPasswordError(ValidationError):
    pass


class UserContactNoError(ValidationError):
    pass


class UserIDNoError(ValidationError):
    pass


class ImageError(ValidationError):
    pass


class DurationError(ValidationError):
    pass


class InvalidKeyError(ValidationError):
    pass
