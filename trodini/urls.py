from django.shortcuts import render
from django.conf.urls import patterns, include, url
from django.contrib import admin


def landing_page(request):
    return render(request, 'index.html')


urlpatterns = patterns(
    '',
    # Examples:
    url(r'^$', landing_page, name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'api/v1/', include('api.urls', namespace='api')),

    url(r'^admin/', include(admin.site.urls)),
)
