from datetime import datetime

from django.db.models import Q
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import list_route

from apps.travels.models import Travel, Location
from apps.users.models import User
from api.serializers.travels import TravelSerializer
from rest_framework.permissions import AllowAny


class TravelViewSet(viewsets.ModelViewSet):

    queryset = Travel.objects.all()
    model = Travel
    serializer_class = TravelSerializer
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        try:
            start_loc_name = request.data.get('startLocName')
            end_loc_name = request.data.get('endLocName')
            start_loc = request.data.get('startLoc')
            end_loc = request.data.get('endLoc')
            start_time = request.data.get('startTime')
            end_time = request.data.get('endTime')
            price = request.data.get('price')
            user_id = request.data.get('user_id', 1)

            end_time = datetime.strptime(
                end_time, '%H:%M:%S').time()
            start_time = datetime.strptime(
                start_time, '%H:%M:%S').time()

            if start_loc:
                start_loc, created = Location.objects.get_or_create(
                    lat_long=start_loc, area_name=start_loc_name)
            else:
                start_loc, created = Location.objects.get_or_create(
                    area_name=start_loc_name)

            if end_loc:
                end_loc, created = Location.objects.get_or_create(
                    lat_long=end_loc, area_name=end_loc_name)
            else:
                end_loc, created = Location.objects.get_or_create(
                    area_name=end_loc_name)

            travel = Travel.objects.create(
                start_time=start_time, end_time=end_time,
                place_of_departure=start_loc, place_of_arrival=end_loc,
                user=User.objects.get(id=user_id), price=float(price))

            travel = TravelSerializer(travel).data

            return Response(
                {'results': travel}, status=status.HTTP_201_CREATED)
        except Exception:
            return Response(
                {'details': 'Could not create travel'},
                status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['POST'])
    def search(self, request, *args, **kwargs):
        '''
        Returns all the travels that start at the start location
        and end at the end location.

        Implement graph theory in future
        '''
        try:
            start_area = request.data.get('startLocName')
            end_area = request.data.get('endLocName')

            travels = Travel.objects.filter(
                Q(place_of_departure__area_name=start_area) |
                Q(place_of_arrival__area_name=end_area))

            travels = TravelSerializer(travels, many=True).data

            return Response(
                {'results': travels}, status=status.HTTP_200_OK)
        except Exception:
            return Response(
                {'details': 'Could not get routes'},
                status=status.HTTP_400_BAD_REQUEST)
