from django.contrib.auth import authenticate as django_authenticate
from django.contrib.auth import login as django_login
from django.contrib.auth import logout as django_logout
from rest_framework import viewsets, status
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.exceptions import PermissionDenied

from api.serializers.full_user import UserSerializer
from apps.users.models import User
from apps.users.permissions import IsOwner
from trodini.utils import literals, errors
from trodini.utils.parsers import parse_error


@api_view(['POST'])
@permission_classes((AllowAny, ))
def login(request, format=None):
    try:
        password = request.data.get(literals.PASSWORD_STRING)
        email = request.data.get(literals.EMAIL_STRING)

        user = django_authenticate(email=email, password=password)
        django_login(request, user)

        user = UserSerializer(user)

        return Response(
            {'results': user.data}, status=status.HTTP_200_OK)

    except AttributeError:
        return Response(
            {literals.DETAILS_STRING: literals.USER_DOES_NOT_EXIST_STRING},
            status=status.HTTP_400_BAD_REQUEST)
    except Exception, e:
        print e
        return Response(
            {literals.DETAILS_STRING: literals.USER_LOGIN_ERROR},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
@permission_classes((AllowAny, ))
def signup(request, format=None):
    try:
        password = request.data.get(literals.PASSWORD_STRING)
        email = request.data.get(literals.EMAIL_STRING)
        first_name = request.data.get(literals.FIRST_NAME_STRING)
        last_name = request.data.get(literals.LAST_NAME_STRING)
        sm_id = request.data.get(literals.SM_ID_STRING)

        user = User.objects.create_user(
            email, first_name, last_name, password)

        if sm_id:
            user.sm_id = sm_id
            user.save()

        user = django_authenticate(email=email, password=password)
        django_login(request, user)

        user = UserSerializer(user)

        return Response(
            {'results': user.data}, status=status.HTTP_200_OK)

    except errors.UserContactNoError, err:
        return parse_error(
            err, literals.CONTACT_NO_STRING,
            literals.USER_SIGNUP_ERROR)
    except errors.UserIDNoError, err:
        return parse_error(
            err, literals.ID_NO_STRING,
            literals.USER_SIGNUP_ERROR)
    except errors.UserPasswordError, err:
        return parse_error(
            err, literals.PASSWORD_STRING, literals.USER_SIGNUP_ERROR)
    except errors.NameError, err:
        return parse_error(
            err, literals.NAME_STRING, literals.USER_SIGNUP_ERROR)
    except Exception, e:
        print e
        return Response(
            {literals.DETAILS_STRING: literals.USER_SIGNUP_ERROR},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
@permission_classes((AllowAny, ))
def logout(request, format=None):
    try:
        django_logout(request)
        return Response(
            literals.USER_LOGOUT_SUCCESS,
            status=status.HTTP_200_OK)
    except Exception:
        return Response(
            literals.ERROR_LOGGING_OUT,
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class UserViewSet(viewsets.ModelViewSet):

    serializer_class = UserSerializer
    model = User
    queryset = User.objects.filter(is_admin=False)
    permission_classes = [IsOwner, IsAuthenticated]

    @detail_route(methods=['POST'])
    def set_password(self, request, pk, *args, **kwargs):
        try:
            password = request.data.get('password')
            user = self.get_object()
            user.set_password(password)
            user.save()

            user = self.serializer_class(user)

            return Response(
                {'results': user.data}, status=status.HTTP_200_OK)
        except PermissionDenied:
            return Response(
                literals.PERMISSION_DENIED_ERROR,
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except Exception:
            return Response(
                literals.SET_PASSWORD_ERROR,
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)
