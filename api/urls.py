from django.conf.urls import patterns, include, url
from rest_framework.routers import DefaultRouter

from views.travels import TravelViewSet
from views.users import UserViewSet, signup, logout, login

router = DefaultRouter()
router.register('users', UserViewSet)
router.register('travels', TravelViewSet)

urlpatterns = patterns(
    '',
    url(r'^docs/', include('rest_framework_swagger.urls')),
    url(r'users/signup', signup, name='signup'),
    url(r'users/login', login, name='login'),
    url(r'users/logout', logout, name='logout'),

    url(r'', include(router.urls))
)
