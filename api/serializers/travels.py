from rest_framework import serializers

from users import ShortUserSerializer
from apps.travels.models import Travel, Location


class LocationSerializer(serializers.ModelSerializer):

    latitude = serializers.ReadOnlyField()
    longitude = serializers.ReadOnlyField()

    class Meta:

        model = Location
        fields = ['id', 'latitude', 'longitude', 'area_name', 'description']


class TravelSerializer(serializers.ModelSerializer):

    place_of_departure = LocationSerializer()
    place_of_arrival = LocationSerializer()
    user = ShortUserSerializer()

    class Meta:

        model = Travel
        fields = [
            'id', 'start_time', 'end_time', 'place_of_departure',
            'place_of_arrival', 'user'
        ]


class ShortTravelSerializer(serializers.ModelSerializer):

    class Meta:

        model = Travel
        fields = [
            'id', 'start_time', 'end_time', 'place_of_departure',
            'place_of_arrival'
        ]
