from rest_framework import serializers

from apps.users.models import User


class ShortUserSerializer(serializers.ModelSerializer):
    '''User serializer without sensitive information'''

    avatar = serializers.ReadOnlyField(source='get_avatar')

    class Meta:

        model = User
        fields = ['id', 'first_name', 'last_name', 'avatar']
