from rest_framework import serializers

from apps.users.models import User
from api.serializers.travels import LocationSerializer, ShortTravelSerializer


class UserSerializer(serializers.ModelSerializer):
    '''User serializer without sensitive information'''

    avatar = serializers.ReadOnlyField(source='get_avatar')
    locations = LocationSerializer(many=True, source='locations_been')
    recent_journeys = ShortTravelSerializer(many=True)

    class Meta:

        model = User
        fields = [
            'id', 'first_name', 'last_name', 'avatar', 'auth_token',
            'sm_id', 'locations', 'recent_journeys'
        ]
